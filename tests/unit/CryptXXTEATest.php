<?php

use PixelFusion\XXTEA\CryptXXTEA;

class CryptXXTEATest extends PHPUnit_Framework_TestCase
{
    public function testMain()
    {
        $text = 'My secret message';

        // Encrypt the text
        $crypt = new CryptXXTEA;
        $encrypted = $crypt->encrypt($text);
        $this->assertNotEmpty($encrypted);

        // Decrypt the encrypted text
        $decrypted = $crypt->decrypt($encrypted);
        $this->assertEquals($decrypted, $text);

        $this->assertEmpty($crypt->encrypt(''));
        $this->assertEmpty($crypt->decrypt(''));
    }
}
